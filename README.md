# Configuration

Editez le fichier `.env` et renseigné le dossier où stocker les données de la base variable `DATABASE_VOLUME_PATH`.
Vous pouvez changer le user et son mot de passe, respectivement `DATABASE_USERNAME` et `DATABASE_PASSWORD`.

Vous pouvez également changer le port utilisé par la panneau d'administration `ADMINISTRATION_WEBAPP_PORT`.
L'application est disponible à l'addresse http://localhost:8081 par défaut. 

# Lancement

Pour démarrer votre environnement de développement ouvrez un terminal dans ce dossier et exécutez `docker-compose up -d`.